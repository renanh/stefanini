package com.processadordocumento.model.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;

import com.processadordocumento.model.util.BaseEntities;

@Entity(name = "configuracao")
@Table(name = "configuracoes")
public class ConfiguracaoEntity extends BaseEntities<Long> {

	private static final long serialVersionUID = -5056026801333843767L;

	@Column(length = 45)
	private String tipoArquivo;
	@Column(length = 45)
	private String appCaptura;
	@CreatedDate
	@Temporal(TemporalType.TIMESTAMP)
	private Date data;

	public String getTipoArquivo() {
		return tipoArquivo;
	}

	public void setTipoArquivo(String tipoArquivo) {
		this.tipoArquivo = tipoArquivo;
	}

	public String getAppCaptura() {
		return appCaptura;
	}

	public void setAppCaptura(String appCaptura) {
		this.appCaptura = appCaptura;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}
}
