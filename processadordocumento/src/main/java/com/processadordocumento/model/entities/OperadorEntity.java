package com.processadordocumento.model.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;

import com.processadordocumento.model.util.BaseEntities;

@Entity(name = "operador")
@Table(name = "operadores")
public class OperadorEntity extends BaseEntities<Long> {

	private static final long serialVersionUID = -5056026801333843767L;

	@Column(length = 45)
	private String nome;
	@Column
	private boolean adm;

}
