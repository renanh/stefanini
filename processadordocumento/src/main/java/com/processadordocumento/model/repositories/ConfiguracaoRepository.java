package com.processadordocumento.model.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;
import java.util.List;
import com.processadordocumento.model.entities.ConfiguracaoEntity;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "config", path = "config")
public interface ConfiguracaoRepository extends JpaRepository<ConfiguracaoEntity, Long> {

	List<ConfiguracaoEntity> findByLastName(@Param("name") String name);
}
