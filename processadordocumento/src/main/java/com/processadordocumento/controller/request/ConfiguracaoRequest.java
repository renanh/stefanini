package com.processadordocumento.controller.request;

import java.util.Date;

public class ConfiguracaoRequest {
	private String tipoArquivo;
	private String appCaptura;
	private String operador;
	private Date data;

	public ConfiguracaoRequest(String tipoArquivo, String appCaptura, String operador, Date data) {
		this.tipoArquivo = tipoArquivo;
		this.appCaptura = appCaptura;
		this.operador = operador;
		this.data = data;
	}

	public String getTipoArquivo() {
		return tipoArquivo;
	}

	public void setTipoArquivo(String tipoArquivo) {
		this.tipoArquivo = tipoArquivo;
	}

	public String getAppCaptura() {
		return appCaptura;
	}

	public void setAppCaptura(String appCaptura) {
		this.appCaptura = appCaptura;
	}

	public String getOperador() {
		return operador;
	}

	public void setOperador(String operador) {
		this.operador = operador;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

}
