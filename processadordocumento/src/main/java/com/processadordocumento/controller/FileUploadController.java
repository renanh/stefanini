package com.processadordocumento.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class FileUploadController {

	private final Logger logger = LoggerFactory.getLogger(FileUploadController.class);

	// Save the uploaded file to this folder
	private static String UPLOADED_FOLDER = "F://temp//";

	@PostMapping("/api/upload")
	// If not @RestController, uncomment this
	// @ResponseBody
	public ResponseEntity<?> uploadFile(@RequestParam("file") MultipartFile uploadfile	) {
		final String[] contentTypes = { "application/pdf", "application/msword", "image/jpeg", "image/tiff" };

		logger.debug("Single file upload!");

		if (uploadfile.isEmpty()) {
			return new ResponseEntity("please select a file!", HttpStatus.OK);
		}

		// Validar se o tipo do arquivo � v�lido.
		
		// N�o entendi se deveria ser na configura��o os tipos aceitados naquela
		// configura��o que poderiamos selecionar, no caso entendi como algo fixo
		
		if (!Arrays.asList(contentTypes).contains(uploadfile.getContentType())) {
			return new ResponseEntity("File must be : " + contentTypes, HttpStatus.OK);
		}

		try {
			saveUploadedFile(uploadfile);

		} catch (IOException e) {
			return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
		}

		return new ResponseEntity("Successfully uploaded - " + uploadfile.getOriginalFilename(), new HttpHeaders(),
				HttpStatus.OK);

	}

	// save file
	private void saveUploadedFile(MultipartFile file) throws IOException {

		String contentType = file.getContentType();
		String fileName = file.getOriginalFilename();

		byte[] bytes = file.getBytes();
		Path path = Paths.get(UPLOADED_FOLDER + file.getOriginalFilename());
		Files.write(path, bytes);

	}
}